<?php
require 'vendor/autoload.php';
include_once('.env');

use Passwords;
use Api;
use Pages;
use Sections;

session_start(); //by default requires session storage

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

if (DB_DBNAME) {
	$config['db']['host']   = DB_HOST;
	$config['db']['user']   = DB_USER;
	$config['db']['pass']   = DB_PASS;
	$config['db']['dbname'] = DB_DBNAME;
}

// Create app
$app = new \Slim\App(['settings' => $config]);

// Get container
$container = $app->getContainer();

if (DB_DBNAME) {
	$container['db'] = function ($c) {
		$db = $c['settings']['db'];
		$pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'],
			$db['user'], $db['pass']);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		return $pdo;
	};
}


// Register provider
$container['flash'] = function () {
	return new \Slim\Flash\Messages();
};

$container['twigPath'] = 'templates';

// Register component on container
$container['view'] = function ($container) {
	$view = new \Slim\Views\Twig($container['twigPath'], [
		'cache' => 'cache',
		'auto_reload' => true,
		'debug' => true
	]);

	// Instantiate and add Slim specific extension
	$router = $container->get('router');
	$uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
	$view->addExtension(new \Slim\Views\TwigExtension($router, $uri));

	return $view;
};

$app->group('/admin', function() {
	$this->get('', function ($request, $response, $args) {
		Password::init($this->db);
		Pages::init($this->db);
		$slug = 'empty';
		$firstPage = Pages::getFirst();
		if ($firstPage) {
			$slug = $firstPage->slug;
		}
		if (Password::isLogged()) {
			return $response->withRedirect($this->router->pathFor('admin_page', ['page' => $slug]));
		} else {
			return $this->view->render($response, 'login.html.twig', [
				'messages' => $this->flash->getMessages()
			]);
		}
	})->setName('admin');


	$this->post('', function ($request, $response, $args) {
		Password::init($this->db);
		$data = $request->getParsedBody();
		$isOk = true;

		if (!isset($data['password'])) {
			$isOk = false;
			$this->flash->addMessage('error', 'Aucun mot de passe n\'a été fourni');
		} elseif (!Password::test(filter_var($data['password'], FILTER_SANITIZE_STRING))) {
			$isOk = false;
			$this->flash->addMessage('error', 'Le mot de passe ne correspond pas');
		}

		if ($isOk) {
			$cookie = Password::setCookie();
		}

		return $response->withRedirect($this->router->pathFor('admin'));
	})->setName('login');

	$this->post('/new_password', function ($request, $response, $args) {
		Password::init($this->db);
		$newPassword = Password::reinitLogin();
		if ($newPassword) {
			if(Password::sendPasswordEmail($newPassword)) {
				$this->flash->addMessage('info', 'Un nouveau mot de passe vous a été envoyé par email, vous le receverez d\ici peu de temps !');
			}
		}

		return $response->withRedirect($this->router->pathFor('admin'));
	})->setName('forget_password');
});

$isLoggedMW = function ($request, $response, $next) {
	Password::init($this->db);
	if (Password::isLogged()) {
		$response = $next($request, $response);
	} else {
		$response = $response->withRedirect($this->router->pathFor('admin'));
	}

	return $response;
};

$app->group('/admin/private', function() {
	$this->get('/pages/{page}', function ($request, $response, $args) {
		Pages::init($this->db);
		$page = Pages::getBySlug($args['page']);

		return $this->view->render($response, 'admin.page.html.twig', [
			'pages' => Pages::getAll(),
			'pageActive' => $page,
			'messages' => $this->flash->getMessages()
		]);
	})
	->setName('admin_page')
	;

	$this->get('/sections/{section}', function ($request, $response, $args) {
		Pages::init($this->db);
		Sections::init($this->db);
		$section = Sections::get($args['section']);

		return $this->view->render($response, 'admin.section.html.twig', [
			'pages' => Pages::getAll(),
			'pageActive' => $section ? $section->getPage() : false,
			'sectionActive' => $section,
			'messages' => $this->flash->getMessages()
		]);
	})
	->setName('admin_section')
	;
})->add($isLoggedMW);


$app->group('/api/private', function() {
	$this->post('/sort', function ($request, $response, $args) {
		Api::init($this->db);
		$parsedBody = $request->getParsedBody();
		$test = Api::sort($parsedBody['objectClass'], $parsedBody['orders']);
		return $response;
	})
	->setName('api_sort')
	;

	$this->post('/post', function ($request, $response, $args) {
		Api::init($this->db);

		$parsedBody = $request->getParsedBody();
		$class = $parsedBody['objectClass'];
		unset($parsedBody['objectClass']);
		$id = $parsedBody['id'];
		unset($parsedBody['id']);
		$redirect = $parsedBody['redirect'];
		unset($parsedBody['redirect']);

		$action = '';
		if (isset($parsedBody['action'])) {
			$action = $parsedBody['action'];
			unset($parsedBody['action']);
		}

		if ($action == 'delete') {
			if (Api::delete($class, $id)) {
				$this->flash->addMessage('success', 'La suppression a été effectuée');
			} else {
				$this->flash->addMessage('error', 'Erreur lors de la suppression');
			}
		} elseif ($id == 'new') {
			if (Api::post($class, $parsedBody)) {
				$this->flash->addMessage('success', 'La création a été effectuée');
			} else {
				$this->flash->addMessage('error', 'Erreur lors de la création');
			}
		} else {
			if (Api::put($class, $id, $parsedBody)) {
				$this->flash->addMessage('success', 'La modification a été effectuée');
			} else {
				$this->flash->addMessage('error', 'Erreur lors de la modification');
			}
		}

		return $response->withRedirect($redirect);
	})
	->setName('api_post')
	;
})->add($isLoggedMW);

$app->get('/sitemap.xml', function ($request, $response, $args) {
	Pages::init($this->db);

	$response = $response->withHeader('Content-type', 'applicationx/ml');
	$pages = Pages::getAll();
	$firstPage = Pages::getFirst();

	$pagesInTheSitemap = [];
	foreach ($pages as $page) {
		if ($page->slug != $firstPage->slug) {
			$pagesInTheSitemap[] = $page;
		}
	}
	return $this->view->render($response, 'sitemap.xml.twig', [
		'pages' => $pagesInTheSitemap,
		'domain' => $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST']
	]);
})
	->setName('sitemap')
;

$app->get('/', function ($request, $response, $args) {
	Pages::init($this->db);
	$firstPage = Pages::getFirst();

	return renderPage($this, $response, $firstPage);
})->setName('home');


$app->get('/{page}', function ($request, $response, $args) {
	Pages::init($this->db);
	$page = Pages::getBySlug($args['page']);
	$firstPage = Pages::getFirst();
	if (!$page || ($page->slug == $firstPage->slug)) {
		return $response->withRedirect($this->router->pathFor('home'));
	}
	return renderPage($this, $response, $page);
})
->setName('page')
;

$app->get('/{page}/{section}', function ($request, $response, $args) {
	Pages::init($this->db);
	Sections::init($this->db);
	$page = Pages::getBySlug($args['page']);
	$section = Sections::getBySlug($page->id, $args['section']);

	return renderPage($this, $response, $page, $section);
})
->setName('page_section')
;

function renderPage($app, $response, $page, $section = null) {
	$template = 'index.html.twig';

	$pageName = $page->slug;
	if ($section) {
		$pageName .= '_'.$section->slug;
	}
	if (file_exists(dirname(__FILE__).'/'.$app['twigPath'].'/pages/'.$pageName.'.html.twig')) {
		$template = 'pages/'.$pageName.'.html.twig';
	}

	$values = [];
	if (function_exists($pageName)) {
		$function = $pageName;
		$values = $function($app, $values);
	}

	$values['pages'] = Pages::getAll();
	$values['pageActive'] = $page;
	$values['sectionActive'] = $section;
	$values['messages'] = $app->flash->getMessages();

	return $app->view->render($response, $template, $values);
}

// Run app
$app->run();
